# -*- coding: utf-8 -*-

"""Top-level package for lz-validation."""

__author__ = """Luke Kreczko"""
__email__ = 'kreczko@cern.ch'
__version__ = '0.0.3'
