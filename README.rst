=============
lz-validation
=============


.. image:: https://img.shields.io/pypi/v/lz_validation.svg
        :target: https://pypi.python.org/pypi/lz_validation

.. image:: https://img.shields.io/travis/kreczko/lz_validation.svg
        :target: https://travis-ci.org/kreczko/lz_validation

.. image:: https://readthedocs.org/projects/lz-validation/badge/?version=latest
        :target: https://lz-validation.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




LZ physics validation package


* Free software: Apache Software License 2.0
* Documentation: https://lz-validation.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
